package pl.edu.agh.estimotetest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import pl.edu.agh.estimotetest.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity {
    private BeaconManager beaconManager;
    private Region region;
    private ListView beaconsListView;
    private ArrayAdapter<String> adapter;
    private LoggingService loggingService;
    private boolean mBound;

    private ServiceConnection logicConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            LoggingService.LocalBinder binder = (LoggingService.LocalBinder) service;
            loggingService = binder.getService();
            mBound = true;
            Toast.makeText(MainActivity.this, "Logging Service Connected!",
                    Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            loggingService = null;
            mBound = false;
            Toast.makeText(MainActivity.this, "Logging Service Disconnected!",
                    Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        beaconsListView = (ListView) findViewById(R.id.beaconsListView);
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1);
        beaconsListView.setAdapter(adapter);
        beaconManager = new BeaconManager(this);
        region = new Region("any", null, null, null);


        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {
                    List<String> discovered = new ArrayList<String>(list.size());
                    StringBuilder csvLine = new StringBuilder();
                    long timestamp = new Date().getTime();
                    csvLine.append(timestamp).append(",");

                    List<Beacon> myList = getSortedBeacons(list);
                    for (Beacon b : myList) {
                        String info = new StringBuilder()
                                .append(b.getMajor()).append(":")
                                .append(b.getMinor()).append("\n")
                                .append(b.getMeasuredPower()).append("\n")
                                .append(b.getRssi())
                                .toString();
                        discovered.add(info);

                        csvLine.append(b.getMajor()).append(":")
                                .append(b.getMinor()).append(",")
                                .append(b.getMeasuredPower()).append(",")
                                .append(b.getRssi()).append(",");
                    }
                    csvLine.setLength(csvLine.length() - 1);
                    if (mBound) {
                        loggingService.log(csvLine.toString());
                    }
                    Log.d("Logged data", csvLine.toString());

                    adapter.clear();
                    adapter.addAll(discovered);
                }
            }


        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBound) {
            this.bindService(new Intent(MainActivity.this, LoggingService.class),
                    logicConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mBound = false;
            this.unbindService(logicConnection);
        }
    }

    @NonNull
    private List<Beacon> getSortedBeacons(List<Beacon> list) {
        List<Beacon> myList = new ArrayList<Beacon>(list);
        Collections.sort(myList, new Comparator<Beacon>() {
            @Override
            public int compare(Beacon lhs, Beacon rhs) {
                if (lhs.getMajor() == rhs.getMajor()) {
                    return Integer.compare(lhs.getMinor(), rhs.getMinor());
                }
                return Integer.compare(lhs.getMajor(), rhs.getMajor());
            }
        });
        return myList;
    }

    @Override
    protected void onResume() {
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);

        super.onPause();
    }

    public void resetLogs(View view) {
        if (mBound) {
            loggingService.reset();
        }
    }

    public void sendLogs(View view) {
        if (mBound) {
            loggingService.send();
        }
    }

    public void cleanFiles(View view) {
        if (mBound) {
            loggingService.clean();
        }
    }
}
