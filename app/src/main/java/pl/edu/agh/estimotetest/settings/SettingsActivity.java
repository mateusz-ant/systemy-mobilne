package pl.edu.agh.estimotetest.settings;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Michal on 2016-05-02.
 */
public class SettingsActivity extends Activity {
    public static String MAIL_KEY = "pref_mail";
    public static String MAX_BATCH_KEY = "pref_max_batch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
