package pl.edu.agh.estimotetest;

import android.app.Application;

import com.estimote.sdk.EstimoteSDK;

public class EstimoteApplication extends Application {

    public static final String APP_ID = "estimotetest-lxt";
    public static final String APP_TOKEN = "6dca02fe3feff61478bb8482f69662b3";

    @Override
    public void onCreate() {
        super.onCreate();
        EstimoteSDK.initialize(this.getApplicationContext(), APP_ID, APP_TOKEN);
        EstimoteSDK.enableDebugLogging(true);
    }
}
