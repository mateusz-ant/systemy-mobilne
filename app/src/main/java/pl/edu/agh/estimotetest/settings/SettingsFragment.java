package pl.edu.agh.estimotetest.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import pl.edu.agh.estimotetest.R;

/**
 * Created by Michal on 2016-05-02.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }

}
