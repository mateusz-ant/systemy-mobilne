package pl.edu.agh.estimotetest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pl.edu.agh.estimotetest.settings.SettingsActivity;

/**
 * Created by Michal on 2016-04-25.
 */
public class LoggingService extends Service {

    private final IBinder mBinder = new LocalBinder();
    private StringBuilder internalData = new StringBuilder();
    private int currentBatch = 0;
    private String timestamp;
    private FileOutputStream fileOut;

    public class LocalBinder extends Binder {
        LoggingService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LoggingService.this;
        }
    }

    public void log(String data) {
        synchronized (this) {
            internalData.append(data);
            internalData.append("\n");
            currentBatch++;
            if (currentBatch >= getMaxBatchSize()) {
                writeAndEmptyCurrentBatch();
            }
        }
    }


    public void reset() {
        synchronized (this) {
            internalData = new StringBuilder();
            timestamp = getCurrentDateString();
            try {
                fileOut.close();
                fileOut = getFileOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void send() {
        synchronized (this) {
            File filelocation = getFile();
            writeAndEmptyCurrentBatch();
            reset();

            Uri path = Uri.fromFile(filelocation);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("vnd.android.cursor.dir/email");
            String to[] = {getPreferences().getString(SettingsActivity.MAIL_KEY, "nouwak@gmail.com")};
            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
            emailIntent.putExtra(Intent.EXTRA_STREAM, path);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Dane " + timestamp);
            emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(emailIntent);

        }
    }

    public void clean() {
        synchronized (this) {
            File folder = this.getExternalFilesDir("csv");
            File[] files = folder.listFiles();
            for (File file : files) {
                file.delete();
            }
        }
    }

    private void writeAndEmptyCurrentBatch() {
        writeData(internalData.toString());
        internalData = new StringBuilder();
        currentBatch = 0;
    }

    private void writeData(String data) {
        try {
            fileOut.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private String getCurrentDateString() {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyHHmmss");
        return format.format(date);
    }

    @NonNull
    private FileOutputStream getFileOutputStream() throws FileNotFoundException {
        return new FileOutputStream(getFile());
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    private Integer getMaxBatchSize() {
        return Integer.valueOf(getPreferences().getString(SettingsActivity.MAX_BATCH_KEY, "100"));
    }

    @NonNull
    private File getFile() {
        return new File(this.getExternalFilesDir("csv"), timestamp + ".csv");
    }

    public LoggingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        timestamp = getCurrentDateString();
        try {
            fileOut = getFileOutputStream();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}